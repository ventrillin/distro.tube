```
     __ __       _______ _______ _______ _____ 
 .--|  |  |_    |       |   _   |       | _   |
 |  _  |   _|   |___|   |.  |   |___|   |.|   |
 |_____|____|    /  ___/|.  |   |/  ___/`-|.  |
                |:  1  \|:  1   |:  1  \  |:  |
                |::.. . |::.. . |::.. . | |::.|
                `-------`-------`-------' `---'
                                               
```

# DistroTube Videos from 2021 (sorted in reverse order)

TITLE: "Hey, DT! Free Linux Distros Are Not Really Free." Plus More Comments.
=> https://odysee.com/@DistroTube:2/hey,-dt!-free-linux-distros-are-not:2

TITLE: My Personal Website Will Now Be A Gemini Capsule
=> https://odysee.com/@DistroTube:2/my-personal-website-will-now-be-a-gemini:3

TITLE: Unboxing The ZSA Moonlander Programmable Keyboard
=> https://odysee.com/@DistroTube:2/unboxing-the-zsa-moonlander-programmable:0

TITLE: Is The Best RSS Reader An Emacs Package?
=> https://odysee.com/@DistroTube:2/is-the-best-rss-reader-an-emacs-package:6

TITLE: The SEC Sues LBRY. Also, Follow Me On Odysee!
=> https://odysee.com/@DistroTube:2/the-sec-sues-lbry.-also,-follow-me-on:9

TITLE: A Quick Look At Manjaro 21.0 "Ornara" Xfce
=> https://odysee.com/@DistroTube:2/a-quick-look-at-manjaro-21.0-ornara-xfce:c

TITLE: GNOME Board Members Must Resign In Disgrace
=> https://odysee.com/@DistroTube:2/gnome-board-members-must-resign-in:c

TITLE: Mob Mentality Threatens The Free Software Movement
=> https://odysee.com/@DistroTube:2/mob-mentality-threatens-the-free:b

TITLE: Creating A Dmenu Script For Web Bookmarks And History
=> https://odysee.com/@DistroTube:2/creating-a-dmenu-script-for-web:b

TITLE: Want DT's Desktop? Deploy My Dotfiles! - DT LIVE
=> https://odysee.com/@DistroTube:2/want-dt's-desktop-deploy-my-dotfiles!-dt:1

TITLE: Stop Pretending "/bin/sh" Is A Real Shell #shorts
=> https://odysee.com/@DistroTube:2/stop-pretending-bin-sh-is-a-real-shell:5

TITLE: Fix Your Mouse Wheel Scrolling Speed With Imwheel
=> https://odysee.com/@DistroTube:2/fix-qutebrowser-mouse-scrolling-with:5

TITLE: Manage Your Virtual Machines With Cockpit
=> https://odysee.com/@DistroTube:2/manage-your-virtual-machines-with:0

TITLE: The MEME Live Stream - DT LIVE
=> https://odysee.com/@DistroTube:2/the-meme-live-stream-dt-live:3

TITLE: Resistance Is Futile, So I'm Back In Qtile
=> https://odysee.com/@DistroTube:2/resistance-is-futile,-so-i'm-back-in:5

TITLE: The Yakuake Drop-Down Terminal For KDE Plasma
=> https://odysee.com/@DistroTube:2/the-yakuale-drop-down-terminal-for-kde:d

TITLE: I See So Much Negativity In The World
=> https://odysee.com/@DistroTube:2/i-see-so-much-negativity-in-the-world:5

TITLE: Solve Problems With Shell Scripting And Dmenu
=> https://odysee.com/@DistroTube:2/solve-problems-with-shell-scripting-and:d

TITLE: "Hey, DT! How Do I Learn The Terminal?" (And Other Questions Answered)
=> https://odysee.com/@DistroTube:2/hey,-dt!-how-do-i-learn-the-terminal:d

TITLE: YouTube Channels I've Enjoyed Watching In 2021
=> https://odysee.com/@DistroTube:2/youtube-channels-i-ve-enjoyed-watching:a

TITLE: Installation And First Look At Mageia 8
=> https://odysee.com/@DistroTube:2/installation-and-first-look-at-mageia-8:2

TITLE: Five Tips For The Openbox Window Manager
=> https://odysee.com/@DistroTube:2/five-tips-for-the-openbox-window-manager:7

TITLE: The Right Wallpaper Is The Key To A Beautiful Desktop
=> https://odysee.com/@DistroTube:2/the-right-wallpaper-is-the-key-to-a:e

TITLE: The Latest Release Of 0 A.D. Looks Amazing!
=> https://odysee.com/@DistroTube:2/the-latest-release-of-0-a-d-looks-2:0

TITLE: Explaining Everything In My XMonad Config
=> https://odysee.com/@DistroTube:2/explaining-everything-in-my-xmonad:f

TITLE: Siduction Is The "Unstable" Debian-Based Linux Distro
=> https://odysee.com/@DistroTube:2/siduction-is-the-unstable-debian-based:4

TITLE: Read Manpages With Less, Bat, Vim or Neovim
=> https://odysee.com/@DistroTube:2/read-manpages-with-less-bat-vim-or:b

TITLE: Many Computer Users Never Run Updates
=> https://odysee.com/@DistroTube:2/many-computer-users-never-run-updates:8

TITLE: Ubuntu, Then Arch. It's The Road So Many Of Us Travel.
=> https://odysee.com/@DistroTube:2/ubuntu-then-arch-it-s-the-road-so-many:9

TITLE: Mailspring Is An Email Client For Windows, Mac And Linux
=> https://odysee.com/@DistroTube:2/mailspring-is-an-email-client-for:b

TITLE: Crypto Currency Rates From The Command Line
=> https://odysee.com/@DistroTube:2/crypto-currency-rates-from-the-command:e

TITLE: JingOS Is An Ipad Inspired Linux Distro
=> https://odysee.com/@DistroTube:2/jingos-is-an-ipad-inspired-linux-distro:1

TITLE: "Ethical" Software Threatens Free And Open Source Software
=> https://odysee.com/@DistroTube:2/ethical-software-threatens-free-and-open:b

TITLE: Some Additional Thoughts About Gemini And Amfora 
=> https://odysee.com/@DistroTube:2/some-additional-thoughts-about-gemini:4 

TITLE: LeftWM Is A Tiling Window Manager Written In Rust
=> https://odysee.com/@DistroTube:2/leftwm-is-a-tiling-window-manager:9

TITLE: Solus Continues To Impress Me With Each Release 
=> https://odysee.com/@DistroTube:2/solus-continues-to-impress-me-with-each:6

TITLE: The 100K Live Stream
=> https://odysee.com/@DistroTube:2/the-100k-live-stream:f

TITLE: Gemini Is What The Web Should Have Been
=> https://odysee.com/@DistroTube:2/gemini-is-what-the-web-should-have-been:9

TITLE: Read Your RSS Feeds With NewsFlash
=> https://odysee.com/@DistroTube:2/read-your-rss-feeds-with-newsflash:4

TITLE: How To Enable DRM Restricted Content In LibreWolf
=> https://odysee.com/@DistroTube:2/how-to-enable-drm-restricted-content-in:d

TITLE: Rust Programs Every Linux User Should Know About
=> https://odysee.com/@DistroTube:2/rust-programs-every-linux-user-should:d

TITLE: Tabliss Is A "New Tab" Plugin For Firefox and Chrome
=> https://odysee.com/@DistroTube:2/tabliss-is-a-new-tab-plugin-for-firefox:4

TITLE: Attention Arch Users! Replace 'Yay' With 'Paru'.
=> https://odysee.com/@DistroTube:2/attention-arch-users-replace-yay-with:b

TITLE: "Hey, DT. Why LibreOffice Instead Of OpenOffice?" Plus Other Questions.
=> https://odysee.com/@DistroTube:2/hey-dt-why-libreoffice-instead-of:3

TITLE: Configuring The Xmobar Panel In Xmonad
=> https://odysee.com/@DistroTube:2/configuring-the-xmobar-panel-in-xmonad:8

TITLE: A First Look At Manjaro Sway Edition
=> https://odysee.com/@DistroTube:2/a-first-look-at-manjaro-sway-edition:8

TITLE: LibreWolf Is A Web Browser For Privacy and Freedom
=> https://odysee.com/@DistroTube:2/librewolf-is-a-web-browser-for-privacy:9

TITLE: DistroToot Is Now Accepting New Members
=> https://odysee.com/@DistroTube:2/distrotoot-is-now-accepting-new-members:f

TITLE: Beginner's Guide To The Linux Terminal
=> https://odysee.com/@DistroTube:2/beginner-s-guide-to-the-linux-terminal:8

TITLE: Banned From Mastodon? #MeToo
=> https://odysee.com/@DistroTube:2/banned-from-mastodon-metoo:f

TITLE: Mozilla No Longer Supports A Free Internet
=> https://odysee.com/@DistroTube:2/mozilla-no-longer-supports-a-free:5

TITLE: Trading Stocks On Linux
=> https://odysee.com/@DistroTube:2/trading-stocks-on-linux:1

TITLE: The Kimafun Dual Wireless Microphone Set
=> https://odysee.com/@DistroTube:2/the-kimafun-dual-wireless-microphone-set:c

TITLE: Kitty Is A Fast And Feature Rich Terminal Emulator
=> https://odysee.com/@DistroTube:2/kitty-is-a-fast-and-feature-rich:e

TITLE: Installation and First Look of Deepin 20.1
=> https://odysee.com/@DistroTube:2/installation-and-first-look-of-deepin-20:6

TITLE: Good Riddance, 2020.  How You Doin', 2021?!  (DT LIVE)
=> https://odysee.com/@DistroTube:2/good-riddance-2020-how-you-doin-2021-dt:e

## Video Categories

=> ../videos/2017-videos.gmi Videos from 2017
=> ../videos/2018-videos.gmi Videos from 2018
=> ../videos/2019-videos.gmi Videos from 2019
=> ../videos/2020-videos.gmi Videos from 2020
=> ../videos/2021-videos.gmi Videos from 2021
